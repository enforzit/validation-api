FROM golang:alpine as builder

WORKDIR /api
RUN apk add --no-cache curl git
COPY . .
RUN go build .

FROM alpine
WORKDIR /api

COPY --from=builder /api/validation-api /api/exec
ARG API_TOKENS
ENV API_TOKENS ${API_TOKENS}

EXPOSE 8080
CMD ["/api/exec"]
