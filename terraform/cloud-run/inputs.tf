variable "gcp_region" {
  type = string
}

variable "project_name" {
  type = string
}

variable "service_name" {
  type = string
}

variable "image_url" {
  type = string
}

variable "domain_endpoint" {
  type = string
}
