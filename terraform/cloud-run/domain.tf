resource "google_cloud_run_domain_mapping" "domain" {
  name     = var.domain_endpoint
  location = var.gcp_region

  metadata {
    namespace = var.project_name
  }

  spec {
    route_name = google_cloud_run_service.service.name
  }
}
