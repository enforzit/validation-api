module "cloud_run" {
  source = "./cloud-run"

  gcp_region      = "europe-west1"
  project_name    = "enforzit"
  service_name    = "validation-api"
  image_url       = "europe-west1-docker.pkg.dev/enforzit/validation-api/api"
  domain_endpoint = "validation.enforz.it"
}
