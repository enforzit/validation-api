package main

import "gitlab.com/enforzit/validation-api/api"

func main() {
	port := 8080
	api.Init(port)
}
