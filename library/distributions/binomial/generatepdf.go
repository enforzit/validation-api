package binomial

import "math"

func (bn Binomial) GeneratePDF() []float64 {
	pdf := make([]float64, bn.Size+1)
	for i := range pdf {
		pdf[i] = bn.GeneratePointPD(float64(i))
	}
	return pdf
}

func (bn Binomial) GeneratePointPD(x float64) float64 {
	r := float64(bn.Size)
	ncr := bn.calculateNCR(x)
	pd := ncr * math.Pow(bn.SuccessProb, r)
	pd = pd * math.Pow(1-bn.SuccessProb, x-r)
	return pd
}

func (bn Binomial) calculateNCR(x float64) float64 {
	r := float64(bn.Size)
	if r > x/2 {
		r = x - r
	}

	ncr := 1.0
	for i := 1.0; i < r+1; i++ {
		ncr *= x - r + i
		ncr /= i
	}
	return ncr
}
