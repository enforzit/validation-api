package binomial

type Binomial struct {
	Size        int
	Total       float64
	Mean        float64
	SuccessProb float64
}

func New(data []float64) Binomial {
	bn := Binomial{}

	bn.Size = len(data) - 1

	for _, v := range data {
		bn.Total += v
	}

	for i, v := range data {
		bn.Mean += float64(i) * (v / bn.Total)
	}

	bn.SuccessProb = bn.Mean / float64(bn.Size)
	return bn
}
