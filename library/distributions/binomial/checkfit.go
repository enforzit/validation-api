package binomial

import "math"

func (bn Binomial) CheckFit(data []float64, percentile float64) []int {
	wp := []int{}
	for i, v := range data {
		fits := bn.CheckPointFit(v, percentile)
		if !fits {
			wp = append(wp, i)
		}
	}
	return wp
}

func (bn Binomial) CheckPointFit(x, percentile float64) bool {
	pd := bn.GeneratePointPD(x)
	gof := math.Pow(x-pd, 2) / pd
	if gof < percentile {
		return true
	}
	return false
}
