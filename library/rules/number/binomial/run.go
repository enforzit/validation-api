package numberbinomial

import (
	"gitlab.com/enforzit/validation-api/library/distributions/binomial"
	"gitlab.com/enforzit/validation-api/library/notifications"
)

func (rl Rule) Run() (notifications.Notifications, error) {
	ns := notifications.New()

	for _, v := range rl.data {
		bn := binomial.New(v.Numbers)

		wp := bn.CheckFit(v.Numbers, v.Percentile)
		if len(wp) != 0 {
			ns.Add(v.ID, "Numbers do not follow binomial distribution", wp)
		}
	}
	return ns, nil
}
