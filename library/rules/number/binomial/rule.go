package numberbinomial

type DataPoint struct {
	ID         string    `json:"id"`
	Numbers    []float64 `json:"values"`
	Percentile float64   `json:"percentile"`
}

type Rule struct {
	Name        string
	Fields      map[string]string
	Description string
	data        []DataPoint
}

func New() Rule {
	return Rule{
		Name: "binomial distribution",
		Description: "Checks if the set of values provided follows " +
			"a binomial distribution.",
		Fields: map[string]string{
			"id":         "string",
			"values":     "number[]",
			"percentile": "number (0.0 < n < 1.0)",
		},
	}
}
