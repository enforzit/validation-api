package numberbinomial

import (
	"encoding/json"
	"errors"
	"io"
)

func (rl *Rule) IngestData(b io.ReadCloser) error {
	dec := json.NewDecoder(b)
	dec.DisallowUnknownFields()
	err := dec.Decode(&rl.data)
	if err != nil {
		return err
	}

	for _, v := range rl.data {
		if v.Percentile < 0.0 || v.Percentile > 1.0 {
			return errors.New("Percentile must be between 0.0 and 1.0")
		}
	}
	return nil
}
