package numberequals

import "gitlab.com/enforzit/validation-api/library/notifications"

func (rl Rule) Run() (notifications.Notifications, error) {
	ns := notifications.New()
	for _, v := range rl.data {
		if wp := equalItems(v.Numbers); len(wp) != 0 {
			ns.Add(v.ID, "The numbers provided are not equal", wp)
		}
	}
	return ns, nil
}

func equalItems(d []float64) []int {
	wp := []int{}
	for i := range d {
		if i < len(d)-1 && d[i] != d[i+1] {
			wp = append(wp, i)
		}
	}
	return wp
}
