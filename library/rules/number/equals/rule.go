package numberequals

type DataPoint struct {
	ID      string    `json:"id"`
	Numbers []float64 `json:"values"`
}

type Rule struct {
	Name        string
	Fields      map[string]string
	Description string
	data        []DataPoint
}

func New() Rule {
	return Rule{
		Name:        "number equals",
		Description: "Checks if all numbers in the given array are equal",
		Fields: map[string]string{
			"id":     "string",
			"values": "number[]",
		},
	}
}
