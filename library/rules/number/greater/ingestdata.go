package numbergreater

import (
	"encoding/json"
	"io"
)

func (rl *Rule) IngestData(b io.ReadCloser) error {
	dec := json.NewDecoder(b)
	dec.DisallowUnknownFields()
	err := dec.Decode(&rl.data)
	return err
}
