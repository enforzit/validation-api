package numbergreater

type DataPoint struct {
	ID      string    `json:"id"`
	Numbers []float64 `json:"values"`
}

type Rule struct {
	Name        string
	Fields      map[string]string
	Description string
	data        []DataPoint
}

func New() Rule {
	return Rule{
		Name: "number greater",
		Description: "Checks if each number in the given array is " +
			"greater than its following number (descending order)",
		Fields: map[string]string{
			"id":     "string",
			"values": "number[]",
		},
	}
}
