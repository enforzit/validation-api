package numberless

type DataPoint struct {
	ID      string    `json:"id"`
	Numbers []float64 `json:"values"`
}

type Rule struct {
	Name        string
	Fields      map[string]string
	Description string
	data        []DataPoint
}

func New() Rule {
	return Rule{
		Name: "number less",
		Description: "Checks if each number in the given array is " +
			"smaller than its following number (ascending order)",
		Fields: map[string]string{
			"id":     "string",
			"values": "number[]",
		},
	}
}
