package numberless

import "gitlab.com/enforzit/validation-api/library/notifications"

func (rl Rule) Run() (notifications.Notifications, error) {
	ns := notifications.New()
	for _, v := range rl.data {
		if wp := ascendingItems(v.Numbers); len(wp) != 0 {
			ns.Add(v.ID, "Not all the numbers provided are in ascending order", wp)
		}
	}
	return ns, nil
}

func ascendingItems(d []float64) []int {
	wp := []int{}
	for i := range d {
		if i < len(d)-1 && d[i] >= d[i+1] {
			wp = append(wp, i)
		}
	}
	return wp
}
