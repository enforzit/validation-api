package notifications

type Notification struct {
	ID        string `json:"id"`
	Message   string `json:"message"`
	Positions []int  `json:"errorPositions"`
}

type Notifications []Notification

func New() Notifications {
	return Notifications{}
}

func (ns *Notifications) Add(id, message string, positions []int) {
	*ns = append(*ns, Notification{
		ID:        id,
		Message:   message,
		Positions: positions,
	})
}
