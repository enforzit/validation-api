package handlers

import (
	"io"

	notifications "gitlab.com/enforzit/validation-api/library/notifications"
	numberequals "gitlab.com/enforzit/validation-api/library/rules/number/equals"
	numbergreater "gitlab.com/enforzit/validation-api/library/rules/number/greater"
	numberless "gitlab.com/enforzit/validation-api/library/rules/number/less"
)

type Rule interface {
	Run() (notifications.Notifications, error)
	IngestData(io.ReadCloser) error
}

type Handler struct {
	Name        string            `json:"name"`
	Endpoint    string            `json:"endpoint"`
	Rule        Rule              `json:"-"`
	Fields      map[string]string `json:"fields"`
	Description string            `json:"description"`
}

type Handlers []Handler

func New() Handlers {
	var numEqualsRule Rule
	numEquals := numberequals.New()
	numEqualsRule = &numEquals

	var numGreaterRule Rule
	numGreater := numbergreater.New()
	numGreaterRule = &numGreater

	var numLessRule Rule
	numLess := numberless.New()
	numLessRule = &numLess

	return Handlers{
		{
			Endpoint:    "/number/equals",
			Name:        numEquals.Name,
			Rule:        numEqualsRule,
			Fields:      numEquals.Fields,
			Description: numEquals.Description,
		},
		{
			Endpoint:    "/number/greater",
			Name:        numGreater.Name,
			Rule:        numGreaterRule,
			Fields:      numGreater.Fields,
			Description: numGreater.Description,
		},
		{
			Endpoint:    "/number/less",
			Name:        numLess.Name,
			Rule:        numLessRule,
			Fields:      numLess.Fields,
			Description: numLess.Description,
		},
	}
}
