package handlers

import (
	"encoding/json"
	"net/http"
)

func (hs Handlers) ServeRoot(w http.ResponseWriter, r *http.Request) {
	b, err := json.Marshal(hs)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("content-type", "application/json")
	w.Write(b)
}
