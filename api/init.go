package api

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/enforzit/validation-api/api/handlers"
	"gitlab.com/enforzit/validation-api/api/middlewares"
)

func Init(port int) {
	r := mux.NewRouter().StrictSlash(true)
	r.Use(middlewares.Authorize)

	hs := handlers.New()
	r.HandleFunc("/", hs.ServeRoot).Methods("GET")
	for _, h := range hs {
		r.HandleFunc(h.Endpoint, handlers.Serve(h.Rule)).Methods("POST")
	}

	p := ":" + strconv.Itoa(port)
	log.Println("Serving on port " + p)
	log.Fatal(http.ListenAndServe(p, r))
}
