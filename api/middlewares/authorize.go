package middlewares

import (
	"net/http"
	"os"
	"strings"
)

func Authorize(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		correctTokens := strings.Split(os.Getenv("API_TOKENS"), " ")
		reqToken := req.Header.Get("authorization")

		for _, v := range correctTokens {
			if v == reqToken {
				next.ServeHTTP(w, req)
				return
			}
		}
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
	})
}
